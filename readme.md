# FrontEnd Learning


這是我開始碰前端有用到，看過的資源跟教學  
稍微用整理一下循序漸進的方式來說明，僅供參考。

**剛開始模仿跟學習很重要  
寫程式、切版  
誰的方式都沒有絕對好或不好  
自己「心態」才是最重要的。**

多看多學習不要自滿，世界上高手真的太多了...

<p style="text-align: right">Nick.</p>

---

## HTML

### 認識HTML

https://www.w3schools.com/html/

```
認識所有HTML的TAG，HTML4 5的TAG如何使用、特性都要非常清楚  
SEO跟HTML也有關。
```

--

## CSS

### 認識 CSS
https://www.w3schools.com/css/

```
跟HTML一樣，基礎要打好！  
```

### Cross Browser
https://caniuse.com/  

```
要會解決跨瀏覽器的問題。
新的css語法可能好用，但要考慮專案訂定的瀏覽器版本斟酌使用。
```


### 認識Sass Scss
http://sass-lang.com/

```
建議要學 command line 的模式，不然後面會蠻痛苦的。
```

### Framework

- Bootstrap
- W3C CSS

```
這是我比較常用的兩個。  
```

### 觀念

#### BEM - CSS命名規則、架構
http://getbem.com/introduction/

```
如何架構一個網站的css也是很重要的，oocss, smacss...等等  
css的命名方式、架構，這都是加速、提升自己的品質、觀念  
css的調整伏度是很高的，要有明確、固定的架構  
修改調整時才能快速的完成。  

建議可以多看別人的網站、看別人的css是怎麼寫的   
或是也可以去看css framework怎麼寫的。
```

--


## Javascript

認識Javascript  
https://www.w3schools.com/js/
https://developer.mozilla.org/zh-TW/docs/Web/JavaScript

### 進階

- Javascript es6、es7
- Typescript

### Plugin, Framework

```
網路上很多大大小小的Plugin，可以找，我只寫了幾個必用的。
```

#### 初階

- JQuery

#### 進階
 
- Angular
- Vue
- React

```
這是前端在做資料呈現的好工具！
會要先學很多概念
MVC、MVVC、Data-Binding
```
我是比較喜歡Angular + JQuery一起用。

#### 觀念

##### HTTP Method
GET、POST  
https://blog.toright.com/posts/1203/%E6%B7%BA%E8%AB%87-http-method%EF%BC%9A%E8%A1%A8%E5%96%AE%E4%B8%AD%E7%9A%84-get-%E8%88%87-post-%E6%9C%89%E4%BB%80%E9%BA%BC%E5%B7%AE%E5%88%A5%EF%BC%9F.html
##### REST/RESTful
https://zh.wikipedia.org/wiki/REST

##### 物件導向
JavaScript 物件導向介紹  
https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript

##### MVC 
https://zh.wikipedia.org/wiki/MVC
##### MVVC
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel
##### MVVM
https://dotblogs.com.tw/ouch1978/2011/04/23/introducing-mvvm

```
反正大概就是一些希望將前後端程式碼分離的概念跟開發設計模式
```

幾乎所有的程式都有物件導向，這很重要！
-------------------------------
```
一樣建議可以多看別人的網站用了什麼Plugin、Framework。  
也多看別人的程式碼是怎麼寫的。
```

--
  
<br>
<br>
當你進入前端自動化管理的時候....就很難回去以前的工作模式了。

## 前端自動化管理

- Grunt
- Gulp
- Webpack

```
Grunt、Gulp會比較好入門，目前比較多人使用gulp。
```

<br>
<br>
往Full Stack Developer前進.  
我原本就是後端工程師...

## Node.js

用javascript就可以寫後端和資料庫連線  
也可以學其他的後端語言（PHP、.NET...等等）。


## 其他輔助工具資源

### 版本控制

#### 連猴子都能懂的Git入門指南〜掌握版本控制
https://backlogtool.com/git-guide/tw/

```
還有很多別的
```

### Command Line

#### 鳥哥的 Linux 私房菜 -- 關於指令應用
http://linux.vbird.org/linux_basic/redhat6.1/linux_06command.php

```
windows也是用的到，指令大同小異
```

### 尋找解決方案

#### CSS-TRICKS
https://css-tricks.com/

#### Stack Overflow
https://stackoverflow.com/

<br><br><br><br><br><br>
by Nick.